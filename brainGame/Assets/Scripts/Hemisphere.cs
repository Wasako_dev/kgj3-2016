﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Hemisphere : MonoBehaviour {
    public BrainCell[] brainCells = new BrainCell[4];
    public Image ColorBrain;
    public Animator hemiStrokeAnimator;
    public Image hemiBackground;

    public List<BrainCell> aliveCells = new List<BrainCell>();
    // Use this for initialization
    void Awake () {
        BrainCell firstCell;
        firstCell = brainCells [ (int)Random.Range(0f, 3.99f) ];
        firstCell.isAlive = true;
        firstCell.GetComponent<Animator>().SetBool("dead", !firstCell.isAlive);
        Debug.Log("ustaw Cell");
        

    }
	
	// Update is called once per frame
	void Update () {
	
	}
    public BrainCell chooseBrainCell()
    {
        aliveCells.Clear();
        BrainCell chosenOne = null;
        
        
        
            for ( int i = 0; i < brainCells.Length; i++)
            {
                if(brainCells[i].isAlive)
                {
                    aliveCells.Add(brainCells[i]);
                }
            }
            if (aliveCells.Count == 0) 
            {
                Debug.Log("Brain is Dead!!!");
			GameManager.instance.GameOverMethod ();
            return null; 

            }
		Vector3 pastposition = Vector3.zero;
		bool canspeedUp = false;
		if (chosenOne != null) {
			canspeedUp = true;
			pastposition = chosenOne.transform.position;
		}
        chosenOne = aliveCells[Random.Range(0, aliveCells.Count)];
        chosenOne.GetComponent<Animator>().SetBool("chosen", true);

        GameManager.instance.activeCell = chosenOne;
        GameManager.instance.keyToPress = chosenOne.CellLetter;

        Debug.Log("chooseBrainCell() " + chosenOne);
        SetColorBrainOpacity();


		if (chosenOne != null) {
			GameManager.instance.CalcLifetime ();
			if (canspeedUp) {
				GameManager.instance.sparkle.speed = (pastposition - chosenOne.transform.position).magnitude / GameManager.instance.cellLifetime;
			}
				GameManager.instance.StartAutodestructionCounting ();
		}
        return chosenOne;

    }

    void SetColorBrainOpacity()
    {
        Color alphaValue = new Color (1f,1f,1f, (float)aliveCells.Count / (float)brainCells.Length);
        Debug.Log("(float)brainCells.Length/(float)aliveCells.Count = " + (float)brainCells.Length / (float)aliveCells.Count);
        ColorBrain.color = alphaValue;
        hemiBackground.color = alphaValue;
    }
    public void flashHemi()
    {
        hemiStrokeAnimator.SetTrigger("pulse");
    }


}


