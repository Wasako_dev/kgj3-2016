﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
    public List<KeyCode> LeftKeys = new List<KeyCode>();
    public List<KeyCode> RightKeys = new List<KeyCode>();

    public Hemisphere leftHemi, rightHemi, activeHemi;
    public KeyCode keyToPress;
    public BrainCell activeCell;
    public KeyCode lastPressedKey;
    public BrainCell[] allBrainCells = new BrainCell[8];
    public AudioSource musicBox;

    public ParticleSystem blood, stars;

    public AudioClip death;
    public AudioClip[] signal = new AudioClip[3];
    public AudioClip menu, gameplay;
    public AudioClip GameOver;
    
    public int brainPoints;

	private bool keydown = false;

    public List<BrainCell> deadCells = new List<BrainCell>();

    public static GameManager instance = null;             
        
	public GameObject LooseScreen;
	public GameObject WinScreen;

	public float cellLifetime = 5.0f;
	public float maxLifetime = 5.0f;
	public float minLifetime = 1.0f;

	public Sparkle sparkle;
	Coroutine autodestructionCoroutine = null;
       
    Vector3 originalCameraPosition;

    public float shakeAmt = 0;

    public Camera mainCamera;

    void Awake()

        {
            if (instance == null)
                instance = this;
            else if (instance != this)
                Destroy(gameObject);

            //DontDestroyOnLoad(gameObject);
            
            //boardScript = GetComponent<BoardManager>();

           // InitGame();
        }
        

        void Start () {

        LeftKeys.Add(KeyCode.Q); 
        LeftKeys.Add(KeyCode.W);
        LeftKeys.Add(KeyCode.E);
        LeftKeys.Add(KeyCode.R);
        LeftKeys.Add(KeyCode.T);
        LeftKeys.Add(KeyCode.A);
        LeftKeys.Add(KeyCode.S);
        LeftKeys.Add(KeyCode.D);
        LeftKeys.Add(KeyCode.F);
        LeftKeys.Add(KeyCode.G);
        LeftKeys.Add(KeyCode.Z);
        LeftKeys.Add(KeyCode.X);
        LeftKeys.Add(KeyCode.C);
        LeftKeys.Add(KeyCode.V);
        

        RightKeys.Add(KeyCode.Y);
        RightKeys.Add(KeyCode.U);
        RightKeys.Add(KeyCode.I);
        RightKeys.Add(KeyCode.O);
        RightKeys.Add(KeyCode.P);
        RightKeys.Add(KeyCode.H);
        RightKeys.Add(KeyCode.J);
        RightKeys.Add(KeyCode.K);
        RightKeys.Add(KeyCode.L);
        RightKeys.Add(KeyCode.N);
        RightKeys.Add(KeyCode.M);
        RightKeys.Add(KeyCode.B);

        changeHemiSide();
        activeCell = activeHemi.chooseBrainCell();
        musicBox = GetComponent<AudioSource>();

        mainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        originalCameraPosition = mainCamera.gameObject.transform.position;


    }

    // Update is called once per frame
    void Update () {
        /*
        if (Input.anyKeyDown)
        {
            if(activeCell.CellLetter == keyToPress)
            {
                Debug.Log("GOOOD");
            }
           // else if()
        }
        */
	}
    

    void OnGUI()
    {
        Event e = Event.current;
		if (e.isKey && (e.type == EventType.KeyDown) && !keydown){
			keydown = true;
            if (e.keyCode == KeyCode.Escape)
            {
                Debug.Log("Exit! " + e.keyCode);
				BackToMenu ();
            }
            else if(e.keyCode == KeyCode.None)
            {
                
                // do nothing
            }
            else if ( e.keyCode == keyToPress) // good key pressed
            {
                mainCamera.GetComponent<Animator>().SetTrigger("score");
                Debug.Log("Good Key! " + e.keyCode);
                pressGoodKey();
                
            }
            else // bad key pressed
            {
                mainCamera.GetComponent<Animator>().SetTrigger("hit");
                Debug.Log("Bad Key !   " + e.keyCode);
                pressBadKey();
                
                
            }
		} else if(e.isKey && (e.type == EventType.KeyUp) && keydown){
			keydown = false;
		}
            

    }
    void changeHemiSide()
    {
        if (activeHemi == leftHemi)
        {
            activeHemi = rightHemi;
        }
        else
        {
            activeHemi = leftHemi;
        }
        Debug.Log("changeHemiSide()");

    }
    void pressGoodKey()
    {
		InterruptAutodestruction ();
        BrainCell bornOne;
        stars.transform.position = new Vector3 (activeCell.transform.position.x, activeCell.transform.position.y, -80);
        
        stars.Play();
        //Instantiate(stars, activeCell.transform);
        activeHemi.flashHemi();
        activeCell.GetComponent<Animator>().SetBool("chosen", false);
        activeCell.CellLetter = GetRandomLetter(activeHemi);
        activeCell.SetLetter(activeCell.CellLetter);

        changeHemiSide();
        activeCell = activeHemi.chooseBrainCell();

        Debug.Log("pressGoodKey()");
        brainPoints++;
        if(brainPoints > 10)
        {
            GameManager.instance.WinMethod();
        }
        activeCell.ASource.PlayOneShot(signal[brainPoints % 3]);


        if (brainPoints > (leftHemi.aliveCells.Count + rightHemi.aliveCells.Count))
        {
            brainPoints = 0;
            for(int i=0; i < allBrainCells.Length; i++)
            {
            
                if (!allBrainCells[i].isAlive)
                {
                    deadCells.Add(allBrainCells[i]);
                }
            }
            if (deadCells.Count == 0)
            {
                Debug.Log("Win!!!");

            }
            else
            {
                bornOne = deadCells[Random.Range(0, deadCells.Count)];
                bornOne.GetComponent<Animator>().SetBool("dead", false);
                bornOne.isAlive = true;
            }

            
        }
    }
    public void pressBadKey()
    {
		activeCell.ASource.PlayOneShot(death, 1f);
        brainPoints = 0;
        blood.transform.position = new Vector3(activeCell.transform.position.x, activeCell.transform.position.y, -80);
        blood.Play();
        //Instantiate(blood, activeCell.transform);
        activeCell.isAlive = false;
        activeCell.GetComponent<Animator>().SetBool("dead", true);
        activeCell = activeHemi.chooseBrainCell();
    }
    
    KeyCode GetRandomLetter(Hemisphere hemiSide)
    {
        
        KeyCode chosenLetter;
        if(hemiSide == rightHemi)
        {
           chosenLetter = RightKeys[Random.Range(0, RightKeys.Count)];
        }
        else
        {
            chosenLetter = LeftKeys[Random.Range(0, LeftKeys.Count)];
        }

        return chosenLetter;
    }

	public void GameOverMethod(){
		StartCoroutine (GameOverCoroutine());
	}

	public void WinMethod(){
		StartCoroutine (WinCoroutine());
	}

	public void BackToMenu(){
		SceneManager.LoadScene (0, LoadSceneMode.Single);
	}

	IEnumerator GameOverCoroutine(){
		yield return new WaitForSeconds (2.0f);
		LooseScreen.SetActive (true);
		Debug.Log ("tu odpal Loose screen");//z tego screena załaduj menu
	}
	IEnumerator WinCoroutine(){
		yield return new WaitForSeconds (1.0f);
		WinScreen.SetActive (true);

		Debug.Log ("tu odpal Loose screen");//z tego screena załaduj menu
	}

	public void CalcLifetime(){
		int livingCells = rightHemi.aliveCells.Count + leftHemi.aliveCells.Count;
		cellLifetime = maxLifetime - ((maxLifetime - minLifetime) / (allBrainCells.Length - 1)) * (livingCells - 1);
	}

	public void StartAutodestructionCounting(){
		InterruptAutodestruction ();
		autodestructionCoroutine = StartCoroutine (autodestruction());

	}

	public void InterruptAutodestruction(){
		if (autodestructionCoroutine != null) {
			StopCoroutine (autodestructionCoroutine);
		}
	}

	IEnumerator autodestruction(){
		yield return new WaitForSeconds (cellLifetime);
		pressBadKey ();
	}  
       
    
    void screenShake()
    {

        shakeAmt = brainPoints * 0.1f;
        InvokeRepeating("CameraShake", 0, .015f);
        Invoke("StopShaking", 0.3f);

    }

    void CameraShake()
    {
        Debug.Log("shake1");
        if (shakeAmt > 0)
        {
            float quakeAmt = 1.2f * Random.value * shakeAmt * 2 - shakeAmt;
            Vector3 pp = mainCamera.transform.position;
            pp.x += quakeAmt;
            quakeAmt = 1.2f * Random.value * shakeAmt * 2 - shakeAmt;
            pp.y += quakeAmt;
            Debug.Log("shake2");
            // can also add to x and/or z
            mainCamera.transform.position = pp;
        }
    }

    void StopShaking()
    {
        CancelInvoke("CameraShake");
        mainCamera.transform.position = originalCameraPosition;
    }
}

