﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BrainCell : MonoBehaviour {
    public bool isAlive;
    public KeyCode CellLetter;
    public enum HeSide { Left, Right }
    public HeSide HemiSide;
    public bool isBrainActive;
    public Animator animator;
    public AudioSource ASource;

    

    // Use this for initialization
    void Start () {
        animator = GetComponent<Animator>();
        //  if(HemiSide == HeSide.Left)
        SetLetter(CellLetter);
        GetComponent<Animator>().SetBool("dead", !isAlive);
        ASource = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
	    if(CellLetter == KeyCode.None)
        {
            gameObject.GetComponentInChildren<Text>().text = ("");
        }
	}

    public void SetLetter ( KeyCode Letter )
    {
        gameObject.GetComponentInChildren<Text>().text = ("" + Letter);
    }

    public KeyCode RandomLetter ( HeSide HSphere )
    {
        return KeyCode.A; // placeholder
    }
    public void checkIfAlive()
    {
        if(isAlive == true)
        {

        }
    }
}
