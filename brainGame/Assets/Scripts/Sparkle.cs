﻿using UnityEngine;
using System.Collections;

public class Sparkle : MonoBehaviour {
    public float sparkleSize = 10;
    public float speed;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(GameManager.instance.activeCell == null){
			Destroy (gameObject);
			return;
		}
        float step = speed * Time.deltaTime;
        transform.position = Vector2.MoveTowards(transform.position, GameManager.instance.activeCell.transform.position, step);
        
        transform.localScale.Set(sparkleSize, sparkleSize, sparkleSize);
    }
}
