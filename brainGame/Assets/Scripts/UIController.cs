﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour {

	public void OnPlayButton(){
		
		Debug.Log ("Play");
		SceneManager.LoadScene (1, LoadSceneMode.Single);
	}

	public void OnExitButton(){
		Debug.Log ("Exit");
		Application.Quit ();
	}
}
